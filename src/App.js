import "./App.css";
import FormComponent from "./component/FormComponent";
import { Helmet } from "react-helmet";
import favicon from "./images/bigDemo.jpg"
function App() {
  return (
    <>
      <Helmet> 
         <link ref="icon" src={favicon}/> 
        <title>Amar Ujala Classified - Amar Ujala Advertisement Booking | Amar Ujala News Paper Official Online Booking | Amar Ujala Paper Ad Rates at amarujala.com</title>
       </Helmet>
      <FormComponent />
    </>
  );
}

export default App;
