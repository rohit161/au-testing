import { Button, MenuItem, TextField, Typography, } from "@material-ui/core";

export const renderText = ({ type, label, color, ...rest }) => (
  <Typography variant={type} color={color} {...rest}>
    {label}
  </Typography>
);

export const renderInputField = ({ name, label, type, state, onChange, rows,disabled,value ,helperText}) => {
  const { data, errors } = state;
  return (
    <TextField
      label={label}
      multiline={rows ? true : false}
      rows={rows ? rows : ""}
      type={type ? type : "text"}
      variant='outlined'
      color='primary'
      size='small'
      margin={"normal"}
      fullWidth={true}
      name={name}
      value={value ? value : data[name]}
      error={errors[name] ? true : false}
      helperText={errors[name] ? errors[name] : helperText}
      onChange={onChange}
      disabled={disabled}
      maxLength="2"
    />
  );
};
export const renderSelect = ({ name, label, options, state, onChange, disabled }) => {
  const { data, errors } = state;
  return (
    <TextField
      disabled={disabled}
      select
      label={label}
      variant='outlined'
      color='primary'
      size='small'
      fullWidth={true}
      name={name}
      value={data[name]}
      style={{marginBottom:"5px"}}
      error={errors[name] ? true : false}
      helperText={errors[name] ? errors[name] : ""}
      onChange={onChange}>
      {options.map((item,i) => (
        <MenuItem key={item.value + i} value={item.value} id={item.id}>
          {item.key}
        </MenuItem>
      ))}
    </TextField>
  );
};

export const renderButton = ({ label, variant, color, fullWidth, onClick, background,disabled,type }) => (
  <Button
    elevation={0}
    type={type ? type :null}
    disabled={disabled}
    style={background ? { background: background, } : { borderRadius: "8px", }}
    variant={variant ? variant : "outlined"}
    color={color ? color : "primary"}
    fullWidth={fullWidth ? fullWidth : false}
    onClick={onClick}>
    {label}
  </Button>
);

