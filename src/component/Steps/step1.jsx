import React, { useState, useEffect, } from "react";
import { Box, Grid, Paper, TextField, MenuItem } from "@material-ui/core";
import { styles } from "../common/styles";
import {
  renderButton,
  renderSelect,
} from "../common/DisplayComponent";
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import Link from '@mui/material/Link';
import { Category, Division, SubDivision, AdvType, TreeDataBW, TreeDataC, PriceTreeData, SubDivisionPersonal, TreeDataCPersonal, TreeDataBWPersonal } from "./data"
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import RowImg from "../../images/textad.jpg"
import CdImg from "../../images/displayad.jpg"
import DCdImg from "../../images/doubleDisplay.jpg"
import FormControl from '@mui/material/FormControl';
import { TreeSelect } from 'antd';
import 'antd/dist/antd.css';
import _ from 'lodash'

const { SHOW_PARENT } = TreeSelect;

const FlexStyle = {
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "space-around",
  alignItems: "flex-start"
}
const FlexStyle1 = {
  display: "flex",
  flexWrap: "wrap",
  alignItems: "center",
  flexDirection: "column",
  margin: "15px 0",
  justifyContent: "space-between",
  textAlign: "center"
}

const accordStyle = {
  width: "100%",
  background: "#FFDB58",
  padding: "5px 20px",
  border: "0",
  borderRadius: "8px",
  marginBottom: "10px"
}
const AccordionStyle = {
  borderRadius: "8px",
  margin: "5px 0",
  boxShadow: "none"
}
const GridContainer = {
  justifyContent: "space-between"
}
const GridInput = { margin: "12px 0" }


const Step1 = ({ state, handleChange, handleNext, data }) => {
  // console.log("data in step1", data)
  const [categoryId, setcategoryId] = useState("0")
  const [category, setcategory] = useState("Matrimonial")
  const [divisionData, setDivisionData] = useState(null)
  const [subdivisionData, setsubDivisionData] = useState(null)
  const [disabled, setDisabled] = useState(false)
  const [extraWord, setExtraWord] = useState("-")
  const [editionName, seteditionName] = useState([]);
  const [adTypeName, setAdTypeName] = useState('');
  const [labelName, setLabelName] = useState("")
  const [packageName, setpackageName] = useState([]);
  const [premiumPrice, setPremiumPrice] = useState("");
  const [treeDataV, setTreeDataV] = useState([]);

  const OnChangeTreeD = (value) => {
    // console.log("value TreeD", value)
    if (value.includes("All Edition")) {
      setTreeDataV(["All Edition"])
    } else {
      setTreeDataV(value)
    }
  }

  useEffect(() => {
    let PackageD = [];
    if (treeDataV) {
      treeDataV.map(x => {
        PackageD.push(x.replace(/UPUK|UP|UK|CUP|NOP/, ""))
      })
    }
    setpackageName(PackageD)
  }, [treeDataV])
  data["packageName"] = [...new Set(packageName)];
  data["edition"] = editionName

  const NoDivsionData = () => {
    if (subdivisionData == null) {
      setDisabled(false)
    } else if (data.subDivision) {
      setDisabled(false)
    } else {
      setDisabled(true)
    }
  }

  const ButtonDisabled = () => {
    if (data.category && data.packageName.length > 0 && data.color && data.adsType && data.division) {
      return NoDivsionData()
    } else {
      setDisabled(true)
    }
  }
  const EditionDisabled = () => {
    if (data.categoryName && data.color && data.adsType) {
      return false
    } else {
      return true;
    }
  }

  const PriceDataFunc = () => {
    if (data.adsType == "Row") {
      data["extraCharge"] = "/ Word"; setAdTypeName("Row"); data["extraChargePrice"] = extraWord
    } else if (data.adsType == "Classified Display") {
      data["extraCharge"] = "/ SQCM"; setAdTypeName("Display")
    } else if (data.adsType == "Double Column Classified Display") {
      data["extraCharge"] = "/ SQCM"; setAdTypeName("Display")
    } else {
      data["extraCharge"] = ""; data["extraChargePrice"] = 0
    }

  }


  let MatrimonialRate = {
    Row: { color: "2020", bw: "1818" },
    Display: { color: "454", bw: "412" },
    Extra: { color: "165", bw: "150" },
  }




  const ProofLabel = () => {
    if (data.division == "Mitrata" || data.division == "Loan/Finance" || data.division == "Mobile Tower" || data.division == "Massage" || data.division == "Beauty Parlour" || data.division == "Sexual Consultation") {
      if (data.division == "Mitrata") {
        setPremiumPrice("*150% premium applicable on this subcategory")
      } else if (data.division == "Sexual Consultation") {
        setPremiumPrice("*25% premium applicable on this subcategory on ROW Advts only")
      } else {
        setPremiumPrice("*50% premium applicable on this subcategory on ROW Advts only")
      }
      setLabelName("*Id Proof is required to book this advertisment");
    } else if (data.subDivision == "Change of Name") {
      setLabelName("*Necessary affidavit document is required to book this advertisment.")
    } else if (data.subDivision == "Disown") {
      setLabelName("*Id Proof(Lawyer letterhead) is required to book this advertisment ")
    } else if (data.subDivision == "Lost & Found") {
      setLabelName("*Necessary document (FIR / NCR) is required to book this advertisment")
    } else if (data.subDivision == "Missing Person") {
      setLabelName("*Necessary document (FIR with ID Proof) is required to book this advertisment")
    } else {
      setLabelName(""); setPremiumPrice("")
    }
  }

  // console.log("Data of ", data)
  useEffect(() => {
    if (data.color || data.categoryName) {
      setTreeDataV([])
    }
  }, [data.color, data.category])


  useEffect(() => {
    let priceSum = 0
    let extraChargeSum = 0
    let personalRowSum = 0
    if (data.packageName.length > 0) {
      if (data.categoryName == "Matrimonial") {
        priceSum += parseInt(MatrimonialRate[adTypeName][data.color]);
        extraChargeSum += parseInt(MatrimonialRate["Extra"][data.color]);
        data["price"] = priceSum;
        setExtraWord(extraChargeSum);
      } else {
        data.packageName.map(x => {
          priceSum += parseInt(PriceTreeData[x.replace(/UPUK|UP|UK|CUP|NOP/, "")][adTypeName][data.color]);
          extraChargeSum += parseInt(PriceTreeData[x.replace(/UPUK|UP|UK|CUP|NOP/, "")]["Extra"][data.color]);
          if (data.adsType == "Row" && data.division == "Personal Info.") {
            personalRowSum += parseInt(PriceTreeData[x.replace(/UPUK|UP|UK|CUP|NOP/, "")]["PersonalRow"][data.color]);
            data["price"] = personalRowSum;
          }else{
            data["price"] = priceSum;
          }
          setExtraWord(extraChargeSum);
        })
      }
    } else {
      data["price"] = "-";
      setExtraWord("-");
    }
  })

  useEffect(() => {
    if (data.category) {
      data["categoryName"] = data.category[0].name
      if (data.category[0].id !== undefined && data.category[0].id == 1) {
        return setcategoryId(data.category[0].id - 1), setcategory(data.category[0].name), setDivisionData(Division[categoryId][category]), setsubDivisionData(SubDivision)
      } else if (data.category[0].id !== undefined) {
        return setcategoryId(data.category[0].id - 1), setcategory(data.category[0].name), setDivisionData(Division[categoryId][category]), setsubDivisionData(null)
      } else {
        setDivisionData(null);
      }
    }
  })


  useEffect(() => {
    ProofLabel()
    ButtonDisabled();
    PriceDataFunc();
    if (data.division == "Personal Info.") {
      setsubDivisionData(SubDivisionPersonal)
    }
  })


  const PackageTreeData = () => {
    if (data.categoryName == "Matrimonial") {
      return TreeDataBW.slice(0, 1)
    } else if (data.division == "Personal Info." && data.adsType == "Row") {
      if (data.color == "color") {
        return TreeDataCPersonal
      } else {
        return TreeDataBWPersonal
      }
    } else {
      if (data.color == "color") {
        if (treeDataV.includes("All Edition")) {
          return TreeDataC.slice(0, 1)
        } else if (treeDataV.includes("Up + Uttrakhand")) {
          return TreeDataC.slice(0, 5)
        } else if (treeDataV.includes("UttarPradesh")) {
          return TreeDataC.slice(0, 6)
        } else {
          return TreeDataC
        }
      } else {
        if (treeDataV.includes("All Edition")) {
          return TreeDataBW.slice(0, 1)
        } else if (treeDataV.includes("North Power")) {
          let FirstArr = TreeDataBW.slice(0, 2)
          let SecondArr = TreeDataBW.slice(8, 12)
          return FirstArr.concat(SecondArr)
        } else if (treeDataV.includes("Up + Uttrakhand")) {
          return TreeDataBW.slice(0, 9)
        } else if (treeDataV.includes("UttarPradesh")) {
          return TreeDataBW.slice(0, 11)
        } else {
          return TreeDataBW
        }
      }
    }
  }

  return (

    <Paper elevation={0} style={styles.steps} p={0}>
      <Accordion elevation={0} defaultExpanded={true} default style={AccordionStyle}>
        <AccordionSummary style={accordStyle}
          expandIcon={<ArrowDropDownIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography variant="p">Select Format</Typography>
        </AccordionSummary>
        <AccordionDetails style={{ padding: "0" }}>
          <Grid container spacing={1} style={{ marginBottom: "20px" }}>
            <Grid item xs={12} sm={12}>
              <RadioGroup style={FlexStyle} onChange={handleChange} value={data['adsType']} id="adsType" name="adsType" row aria-label="adsType" >
                <Grid item xs={4} sm={4}>
                  <FormControlLabel className="adstype-label step3-label" size={"medium"} style={FlexStyle1} value="Row" control={<Radio />} label={<><img style={{ width: "60px", marginBottom: "5px" }} src={RowImg} /><br /> Row</>} icon={RowImg} />
                </Grid>
                <Grid item xs={4} sm={4}>
                  <FormControlLabel className="adstype-label step3-label" size={"medium"} style={FlexStyle1} value="Classified Display" control={<Radio />} label={<><img style={{ width: "60px", marginBottom: "5px" }} src={CdImg} /><br /> Classified <br />Display</>} />
                </Grid>
                <Grid item xs={4} sm={4}>
                  <FormControlLabel className="adstype-label step3-label" size={"medium"} style={FlexStyle1} value="Double Column Classified Display" control={<Radio />} label={<><img style={{ width: "60px", marginBottom: "5px" }} src={DCdImg} /><br /> Double Column<br /> Classified Display</>} />
                </Grid>
              </RadioGroup>
            </Grid>
          </Grid>
        </AccordionDetails>
      </Accordion>
      <Accordion elevation={0} style={AccordionStyle} defaultExpanded={true}>
        <AccordionSummary
          expandIcon={<ArrowDropDownIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header" style={accordStyle}
        >
          <Typography variant="p">Category</Typography>
        </AccordionSummary>
        <AccordionDetails>

          <Grid container spacing={1} style={GridContainer}>

            <Grid item xs={12} sm={5} style={GridInput}>
              {renderSelect({
                state,
                name: "category",
                label: "Select Categories",
                options: Category,
                onChange: handleChange,
              })}
            </Grid>
            <Grid item xs={12} sm={5} style={GridInput}>
              <TextField
                select
                // disabled={divDisable}
                label={"Select Division"}
                variant='outlined'
                color='primary'
                size='small'
                fullWidth={true}
                name={"division"}
                value={data['division']}
                style={{ marginBottom: "5px" }}
                onChange={handleChange}>
                {divisionData ? divisionData.map((item, i) => (
                  <MenuItem key={item.value + "division" + i} value={item.value} id={item.id}>
                    {item.key}
                  </MenuItem>
                )) :
                  <MenuItem key="None" value="None">
                    No Division
                  </MenuItem>
                }
              </TextField>

            </Grid>
          </Grid>
          <Grid container spacing={1} style={GridContainer}>
            <Grid item xs={12} sm={5} style={GridInput}>
              <TextField
                select
                label={"Select Sub-Division"}
                variant='outlined'
                color='primary'
                size='small'
                fullWidth={true}
                name={"subDivision"}
                value={data['subDivision']}
                style={{ marginBottom: "5px" }}
                onChange={handleChange}>
                {subdivisionData !== null ? subdivisionData.map((item, i) => (
                  <MenuItem key={item.value + i + "subdivision"} value={item.value} id={item.id}>
                    {item.key}
                  </MenuItem>
                )) :
                  <MenuItem key="None" value="None">
                    No Sub-Division
                  </MenuItem>
                }
              </TextField>
            </Grid>
            <Grid item xs={12} sm={5} >
              {labelName ? <p style={{ color: "rgb(209, 32, 35)", width: "100%", marginTop: "8px", fontWeight: "bold" }}>{labelName}</p> : null}
              {premiumPrice ? <p style={{ color: "rgb(209, 32, 35)", width: "100%", marginTop: "8px", fontWeight: "bold" }}>{premiumPrice}</p> : null}
            </Grid>
          </Grid>
        </AccordionDetails>
      </Accordion>

      <Accordion elevation={0} defaultExpanded={true} style={AccordionStyle}>
        <AccordionSummary style={accordStyle}
          expandIcon={<ArrowDropDownIcon />}
          aria-controls="panel3a-content"
          id="panel3a-header"
        >
          <Typography variant="p">Suggested Packages</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Grid container spacing={1} style={GridContainer}>
            <Grid item xs={12} sm={5} style={GridInput}>
              {renderSelect({
                state,
                name: "color",
                label: "Select Adv Type",
                options: AdvType,
                onChange: handleChange,
              })}
            </Grid>
            <Grid item xs={12} sm={5} style={{ ...GridInput, position: "relative" }}>
              {treeDataV.length > 0 ? <span style={{ position: "absolute", top: "-8px", left: "12px", zIndex: "500", color: "rgba(0, 0, 0, 0.54)", padding: "0 10px", background: "white", fontSize: "12px" }}>Select Package</span> : null}
              <svg style={{ zIndex: "50", top: "calc(25% - 12px)" }} className="MuiSvgIcon-root MuiSelect-icon MuiSelect-iconOutlined" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M7 10l5 5 5-5z"></path></svg>
              <FormControl sx={{ width: "100%", marginBottom: "10px" }}>
                <TreeSelect
                  treeData={PackageTreeData()}
                  value={treeDataV}
                  onChange={OnChangeTreeD}
                  treeCheckable={true}
                  showCheckedStrategy={SHOW_PARENT}
                  placeholder={"Select Package"}
                  style={{ width: "100%", }}
                  // maxTagCount={2}
                  // maxTagPlaceholder={"..."}
                  disabled={EditionDisabled()}
                />
              </FormControl>
              <Link href="https://docs.google.com/spreadsheets/d/1u5j7E8bBdac3oKpq-7OI9maYJkCXFpeD/edit?usp=sharing&ouid=115284666258794638570&rtpof=true&sd=true" underline="always" target="blank">
                Click here to view the edition coverage documents
              </Link>
            </Grid>

          </Grid>
          <Grid container component={Box} justify='space-around' mt={3} p={2} style={{ background: "black", color: "white", borderRadius: "8px 8px 0px 0px" }}>
            <Grid className="step3-label text-center" xs={4}>Package / Edition</Grid>
            <Grid className="step3-label text-center" xs={4}>Basic Ad Cost</Grid>
            {/* <Grid className="step3-label text-center" xs={4}>Extra Charges</Grid>  */}
          </Grid>
          <Grid container component={Box} justify='space-around' p={2} style={{ background: "#f9f9f9" }}>
            <Grid className="step3-label text-center" xs={4}>{data.packageName.join(', ')}</Grid>
            <Grid className="step3-label text-center" xs={4} >{data.price ? data.price.toLocaleString():"-"}</Grid>
            {/* <Grid className="step3-label text-center" xs={4}>{data.adsType == "Row" ? extraWord + extraCharge : "NA"}</Grid> */}
          </Grid>

        </AccordionDetails>

      </Accordion>
      <Grid container component={Box} justify='flex-end' mt={2} p={2} style={{ margin: "20px 0" }}>
        {renderButton({ label: "Compose Your Ad", disabled: disabled, onClick: handleNext, variant: "contained", background: "#D12023" })}
      </Grid>

    </Paper>
  );
};

export default Step1;
