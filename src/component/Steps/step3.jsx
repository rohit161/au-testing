import React, { useState, useEffect } from "react";
import { Box, Grid, Paper, TextField } from "@material-ui/core";
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import { styles } from "../common/styles";
import { renderButton, renderInputField, } from "../common/DisplayComponent";

const Step3 = ({
  state,
  handleChange,
  handleNext,
  handlePrev,
  data,
  handleSubmit,
  DocHandler,
}) => {
  const [totalPrice, setTotalPrice] = useState("")
  const [gstTotal, setgstTotal] = useState("");
  const [colorName, setcolorName] = useState("Color");
  const [eyeCatcherPrice, setEyeCatcherPrice] = useState(0)
  const [adOnPrice, setAdOnPrice] = useState(0)
  const [disabled, setDisabled] = useState(true);
  const [adCost, setAdCost] = useState(0)
  const [PremiumadCost, setPremiumadCost] = useState("")
  const [labelName, setLabelName] = useState("")
  const [extraWordCost, setExtraWordCost] = useState(0)
  // console.log("price Data",adCost,adOnPrice,gstTotal,totalPrice,eyeCatcherPrice)

  const InformationLHS = [
    { title: "Format:", value: data.adsType },
    { title: "Division:", value: data.division },
    // { title: "Edition / Package:", value: data.packageName.join(', ') },

  ]
  const InformationRHS = [
    // { title: "Category:", value: data.category[0].name },
    { title: "Sub Division:", value: data.subDivision },
    { title: "Advt Type:", value: colorName },
    // { title: "Sizes:", value: `"H" + ${data.height} + "cm x W" + ${data.width} + "cm"` },
  ]
  const BtnDisabled = () => {
    if (data.name && data.email && data.mobile && data.address) {
      if (data.division == "Mitrata" || data.division == "Loan/Finance" || data.division == "Mobile Tower" || data.division == "Sexual Consultation" || data.subDivision == "Change of Name" || data.subDivision == "Disown" || data.subDivision == "Lost & Found" || data.subDivision == "Missing Person") {
        if (data.idProofName) {
          setDisabled(false)
        } else {
          setDisabled(true)
        }
      }
      else {
        setDisabled(false)
      }
    }
  }

  const PremiumBasicAdCost = () => {
    if (data.division == "Mitrata") {
      data["PremiumAdCost"] = Math.round((150 / 100) * (adCost), 2);setPremiumadCost("(150%)")
    } else if (data.adsType == "Row") {
      if (data.division == "Loan/Finance" || data.division == "Mobile Tower" || data.division == "Massage" || data.division == "Beauty Parlour") {
        data["PremiumAdCost"] = Math.round((50 / 100) * (adCost), 2);setPremiumadCost("(50%)")
      } else if (data.division == "Sexual Consultation") {
        data["PremiumAdCost"] = Math.round((25 / 100) * (adCost), 2);setPremiumadCost("(25%)")
      }  else {
        data["PremiumAdCost"] = 0;setPremiumadCost("")
      }
    }else{
      data["PremiumAdCost"] = 0;setPremiumadCost("")
    }
  }

  const AdCostFunc = () => {
    if (data.categoryName == "Matrimonial") {
      if (data.date.length == 3) {
        setAdCost(data.price * 2);
      } else {
        setAdCost(data.price * data.date.length)
      }
    } else if (data.categoryName != "Matrimonial") {
      if (data.date.length == 3) {
        setAdCost(data.price * 2)
      } else if (data.date.length == 8) {
        setAdCost(data.price * 5);
      } else if (data.date.length == 30) {
        setAdCost(data.price * 15);
      } else {
        setAdCost(data.price * data.date.length)
      }
    }
  }
  const GSTCheck = () => {
    if (data.BillCompanyName) {
      if (data.gst) {
        return setDisabled(false)
      } else {
        return setDisabled(true)
      }
    }
  }
  const EyeCatcherPrice = () => {
    data['totalPrice'] = totalPrice
    if (data.eyeCatcher == "tick-mark" || data.eyeCatcher == "heart") {
      setEyeCatcherPrice(50)
    }
  }

  const PriceFunc = () => {
    if (data.Bold && data.Border) {
      setAdOnPrice(Math.round((60 / 100) * (adCost), 2));
    } else if (data.Border) {
      setAdOnPrice(Math.round((25 / 100) * (adCost), 2));
    } else if (data.Bold) {
      setAdOnPrice(Math.round((35 / 100) * (adCost), 2));
    }
  }
  const ColorNameFunc = () => {
    if (data.color == "color") {
      setcolorName("Color")
    } else {
      setcolorName("Black and White")
    }
  }
  const ExtraWordFunc = () => {
    let extraWord = 0
    if (data.adsType == "Row") {
      if (data.wordCount > 50) {
        extraWord = data.wordCount - 50;
        setExtraWordCost(extraWord * data.extraChargePrice)
      }
    }
  }
  const ProofLabel = () => {
    if (data.division == "Mitrata" || data.division == "Loan/Finance" || data.division == "Mobile Tower" || data.division == "Massage" || data.division == "Beauty Parlour" || data.division == "Sexual Consultation") {
       setLabelName("Please upload your Id Proof")
    } else if (data.subDivision == "Change of Name") {
       setLabelName("Please upload necessary affidavit document.")
    } else if (data.subDivision == "Disown") {
       setLabelName("Please upload your Id Proof (Lawyer letterhead)")
    } else if (data.subDivision == "Lost & Found") {
       setLabelName("Please upload necessary document (FIR / NCR)")
    } else if (data.subDivision == "Missing Person") {
       setLabelName("Please upload necessary document (FIR with ID Proof)")
    }
  }


  const GSTPrice = Math.round((5 / 100) * (gstTotal), 2)
  useEffect(() => {
    AdCostFunc();
    PremiumBasicAdCost();
    PriceFunc();
    ExtraWordFunc();
    setgstTotal(adOnPrice + adCost + eyeCatcherPrice + extraWordCost + data["PremiumAdCost"]);
    setTotalPrice(GSTPrice + adOnPrice + adCost + eyeCatcherPrice + extraWordCost +data["PremiumAdCost"]);
    EyeCatcherPrice();
    BtnDisabled();
    GSTCheck();
    ColorNameFunc();
    data['gstPrice'] = GSTPrice;
    data['adOnPrice'] = adOnPrice;
  })

  const NumberwithComma = (x) => {
    // return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",")
    return x.toLocaleString();
  }


  useEffect(() => {
    if (data.idProofName) {
      setLabelName(data.idProofName)
    } else {
      ProofLabel()
    }
  })
  console.log("Data",data,adOnPrice,adCost,eyeCatcherPrice,extraWordCost,data.PremiumAdCost)
  return (
    <Paper elevation={0} style={styles.steps}>
      <Grid container component={Box} mt={2} p={2} style={{ background: "#FFDB58", borderRadius: "8px 8px 0px 0px" }}>
        <Grid>Bill Summary</Grid>
      </Grid>

      <Grid container component={Box} justify='space-around' xs={12} lg={12} mt={6} p={2} style={{ background: "#f9f9f9", borderRadius: "15px" }}>
        <Grid container m={1} component={Box} justify='space-around' rowSpacing={2}>
          <Grid xs={7} lg={10} className="step3-label" ><strong>Edition / Package Covered: </strong><br />{data.packageName.join(" , ")}</Grid>
          <Grid xs={3} lg={2} className="step3-label opacity-step3" ></Grid>
        </Grid>
      <Grid container m={1} component={Box} justify='space-around' rowSpacing={2}>
          <Grid xs={7} lg={10} className="step3-label" ><strong>Category </strong></Grid>
          <Grid xs={3} lg={2} className="step3-label opacity-step3" >{data.categoryName}</Grid>
        </Grid>
      <Grid container m={1} component={Box} justify='space-around' rowSpacing={2}>
          <Grid xs={7} lg={10} className="step3-label" ><strong>Advt Format </strong></Grid>
          <Grid xs={3} lg={2} className="step3-label opacity-step3" >{data.adsType}</Grid>
        </Grid>
      <Grid container m={1} component={Box} justify='space-around' rowSpacing={2}>
          <Grid xs={7} lg={10} className="step3-label" ><strong>Advt Type </strong></Grid>
          <Grid xs={3} lg={2} className="step3-label opacity-step3" >{colorName}</Grid>
        </Grid>
        {data.adsType == "Row" ?
            <Grid container component={Box} justify='space-around' m={1} rowSpacing={2} >
              <Grid xs={7} lg={10} className="step3-label" ><strong>Word Count</strong></Grid>
              <Grid xs={3} lg={2} className="step3-label opacity-step3" >{data.description.match(/(\w+)/g).length}</Grid>
            </Grid>
            :
            <Grid container component={Box} justify='space-around' m={1} rowSpacing={2} >
              <Grid xs={7} lg={10} className="step3-label" ><strong>Size</strong></Grid>
              <Grid xs={3} lg={2} className="step3-label opacity-step3" >H {data.height}cm x W 3cm</Grid>
            </Grid>
          }
        <Grid container m={1} component={Box} justify='space-around' rowSpacing={2}>
          <Grid xs={7} lg={10} className="step3-label" ><strong>Basic Ad Cost: </strong></Grid>
          <Grid xs={3} lg={2} className="step3-label opacity-step3" >Rs.{NumberwithComma(adCost)}</Grid>
        </Grid>
        {data.PremiumAdCost ? <Grid container m={1} component={Box} justify='space-around' rowSpacing={2}>
          <Grid xs={7} lg={10} className="step3-label" ><strong>Additional Premium Cost: </strong>{PremiumadCost}</Grid>
          <Grid xs={3} lg={2} className="step3-label opacity-step3" >Rs.{NumberwithComma(data.PremiumAdCost)}</Grid>
        </Grid>: null}

        {data.adsType == "Row" && data.wordCount > 50 ?
          <Grid container m={1} component={Box} justify='space-around' rowSpacing={2}>
            <Grid xs={7} lg={10} className="step3-label" ><strong>Extra Word Cost</strong>  </Grid>
            <Grid xs={3} lg={2} className="step3-label opacity-step3" >Rs.{NumberwithComma(extraWordCost)}</Grid>
          </Grid>
          : null}
        {data.adsType == "Row" && data.adOnPrice != 0 ?
          <Grid container m={1} component={Box} justify='space-around' rowSpacing={2}>
            <Grid xs={7} lg={10} className="step3-label" ><strong>Ad-On Cost:</strong>  <span style={{ wordBreak: "break-word" }} className={"opacity-step3" + data.Bold || data.Border ? "ads-on" : "no-ads-on"}>{data.Bold ? "(Bold - 35%)" : ""} {data.Bold && data.Border ? "," : ""} {data.Border ? "(Border - 25%)" : ""}</span></Grid>
            <Grid xs={3} lg={2} className="step3-label opacity-step3" >Rs.{NumberwithComma(adOnPrice)}</Grid>
          </Grid>
          : null}
        {data.adsType == "Row" && data.eyeCatcher ?
          <Grid container m={1} component={Box} justify='space-around' rowSpacing={2}>
            <Grid xs={7} lg={10} className="step3-label" ><strong>EyeCatcher:</strong> {data.eyeCatcher.toLowerCase().replace(/\w/, firstLetter => firstLetter.toUpperCase())}</Grid>
            <Grid xs={3} lg={2} className="step3-label opacity-step3" >Rs.{NumberwithComma(eyeCatcherPrice)}</Grid>
          </Grid>
          : null}
        <Grid container m={1} component={Box} justify='space-around' rowSpacing={2}>
          <Grid xs={7} lg={10} className="step3-label" ><strong>GST 5%</strong></Grid>
          <Grid xs={3} lg={2} className="step3-label opacity-step3" >Rs.{NumberwithComma(GSTPrice)}</Grid>
        </Grid>
        <Grid container component={Box} style={{ justifyContent: "flex-end", margin: "5px 5px 5px 0" }} xs={12} lg={12} rowSpacing={2}>
          <Grid xs={3} sm={2} lg={1} md={1}><strong>Total</strong></Grid>
          <Grid className="total-grid-price" lg={2} style={{ paddingRight: "5px" }}> Rs.{NumberwithComma(totalPrice)}  </Grid>
        </Grid>
      </Grid>
      {/* <Grid container component={Box} mt={6} p={2} style={{ background: "#FFDB58", borderRadius: "8px 8px 0px 0px" }}>
        <Grid>Other Information</Grid>
      </Grid>
      <Grid container component={Box} justify='space-around' mt={6} p={2} style={{ background: "#f9f9f9", borderRadius: "15px" }}>
        <Grid lg={6} sm={6} xs={12} container>
          {InformationLHS.map((x, i) => {
            return (
              <Grid container key={"information-lhs" + i} component={Box} justify='space-around' m={1} rowSpacing={2} >
                <Grid lg={4} sm={5} xs={5} className="step3-label" ><strong>{x.title}</strong></Grid>
                <Grid lg={7} sm={7} xs={6} className="step3-label opacity-step3" style={{ wordBreak: "break-word" }}>{x.value}</Grid>
              </Grid>
            )
          })}
        </Grid>
        <Grid lg={6} sm={6} xs={12} container>
          {InformationRHS.map((x, i) => {
            return (
              <Grid key={"information-rhs" + i} container component={Box} justify='space-around' m={1} rowSpacing={2} >
                <Grid lg={3} sm={4} xs={5} className="step3-label" ><strong>{x.title}</strong></Grid>
                <Grid lg={8} sm={8} xs={6} className="step3-label opacity-step3" >{x.value}</Grid>
              </Grid>
            )
          })}
          {data.adsType == "Row" ?
            <Grid container component={Box} justify='space-around' m={1} rowSpacing={2} >
              <Grid lg={3} sm={4} xs={5} className="step3-label" ><strong>Word Count</strong></Grid>
              <Grid lg={8} sm={8} xs={6} className="step3-label opacity-step3" >{data.description.match(/(\w+)/g).length}</Grid>
            </Grid>
            :
            <Grid container component={Box} justify='space-around' m={1} rowSpacing={2} >
              <Grid lg={3} sm={4} xs={5} className="step3-label" ><strong>Size</strong></Grid>
              <Grid lg={8} sm={8} xs={6} className="step3-label opacity-step3" >H {data.height}cm x W 3cm</Grid>
            </Grid>
          }
        </Grid>

      </Grid> */}

      <Grid container component={Box} mt={6} p={2} style={{ background: "#FFDB58", borderRadius: "8px 8px 0px 0px" }}>
        <Grid>User Information</Grid>
      </Grid>
      <Grid container component={Box} justify='space-between' p={2} style={{ textAlign: "center", marginBottom: "20px" }}>
        <Grid xs={12} sm={5}>
          <Grid>
            {renderInputField({
              state,
              name: "name",
              label: "Name",
              onChange: handleChange,
            })}
            {renderInputField({
              state,
              name: "mobile",
              label: "Mobile",
              onChange: handleChange,
              type: "number",
            })}
            {renderInputField({
              state,
              name: "address",
              label: "Address",
              rows: "5",
              onChange: handleChange,
            })}

          </Grid>

        </Grid>
        <Grid xs={12} sm={5}>
          <Grid>
            {renderInputField({
              state,
              name: "email",
              label: "Email",
              onChange: handleChange,
            })}
            {renderInputField({
              state,
              name: "gst",
              label: "GST",
              onChange: handleChange,
            })}
            <Grid>
              <FormGroup mt={2}>
                <FormControlLabel className="checkbox-label" control={<Checkbox onChange={handleChange} name={"BillCompanyName"} />} label="Check if you need bill in your company name, provide respective GST number" />
              </FormGroup>
            </Grid>
            {data.division == "Mitrata" || data.division == "Loan/Finance" || data.division == "Mobile Tower" || data.division == "Sexual Consultation" || data.subDivision == "Change of Name" || data.subDivision == "Disown" || data.subDivision == "Lost & Found" || data.division == "Massage" || data.division == "Beauty Parlour" || data.subDivision == "Missing Person" ?
              <Grid container className="idProofGrid" style={{ justifyContent: "space-between", alignItems: "center" }}>
                <TextField
                  // label={"Img/PDF"}
                  variant='outlined'
                  color='primary'
                  size='small'
                  margin={"normal"}
                  fullWidth={true}
                  name={"idproof"}
                  value={data["idProofName"] ? "File Uploaded Successfully" : labelName}
                  // onChange={handleChange}
                  disabled
                />
                <p style={{ display: "block", width: "100%", textAlign: "left" }}>Note : Attachment must be in PDF/JPEG only</p>
                <Grid>
                  <input type="file" multiple="multiple" accept="application/Image,application/pdf" name="idproof" id="input" onChange={DocHandler} />
                  <div className="label border-radius-8" style={{ padding: "10px", background: "#000", color: "white", marginBottom: "10px" }}>
                    <label className="image-upload" htmlFor="input" >
                      Choose File
                    </label>
                  </div>
                </Grid>

              </Grid> : null
            }

          </Grid>
        </Grid>
        <Grid container component={Box} justify='space-between' mt={2} rowSpacing={2} >
          <Box ml={2} mt={2}>
            {renderButton({
              label: "Back",
              color: "default",
              onClick: handlePrev,
            })}
          </Box>
          <Box ml={2} mt={2}>
            {renderButton({ label: "Continue Payment", disabled: disabled, onClick: handleSubmit, variant: "contained", background: "#D12023" })}
          </Box>
        </Grid>
      </Grid>

    </Paper >
  );
};

export default Step3;
