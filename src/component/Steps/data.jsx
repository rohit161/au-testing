export const Category = [
  { key: "Matrimonial", value: [{ name: "Matrimonial", id: "1" }] },
  { key: "Recruitment", value: [{ name: "Recruitment", id: "2" }] },
  { key: "Services", value: [{ name: "Services", id: "3" }] },
  { key: "Sale", value: [{ name: "Sale", id: "4" }] },
  { key: "Medical", value: [{ name: "Medical", id: "5" }] },
  { key: "Business", value: [{ name: "Business", id: "6" }] },
  { key: "Education", value: [{ name: "Education", id: "7" }] },
  { key: "Information", value: [{ name: "Information", id: "8" }] },
  { key: "Lease/Rent", value: [{ name: "Lease", id: "9" }] },
  // { key: "Local Retail", value: "Local retail" },
  // { key: "Purchase", value: "Purchase" },
]

export const SubDivision = [
  { key: "Brahmin", value: "Brahmin" },
  { key: "Jat", value: "Jat" },
  { key: "Jatav", value: "Matrimonial" },
  { key: "Muslim", value: "Muslim" },
  { key: "Kshatriya", value: "Kshatriya" },
  { key: "Vaish", value: "Vaish" },
  { key: "Christian", value: "Christian" },
  { key: "Punjabi", value: "Punjabi" },
  { key: "Rajput", value: "Rajput" },
  { key: "Kayasth", value: "Kayasth" },
  { key: "Caste No Bar", value: "Caste No Bar" },
  { key: "Yadav", value: "Yadav" },
  { key: "Bengali", value: "Bengali" }
]
export const SubDivisionPersonal = [
  { key: "Disown", value: "Disown" },
  { key: "Change of Name", value: "Change of Name" },
  { key: "Lost & Found", value: "Lost & Found" },
  { key: "Missing Person", value: "Missing Person" },
]
export const AdvType = [
  { key: "Black and White", value: "bw" },
  { key: "Color", value: "color" }
]
export const Division = [
  {
    Matrimonial: [
      { key: "Bride Wanted", value: "Bride Wanted" },
      { key: "Groom Wanted", value: "Groom Wanted" },
      { key: "Marriage Bureau", value: "Marriage Bureau" },
      { key: "Parichay Samelan", value: "Parichay Samelan" },
    ]
  },
  {
    Recruitment: [
      { key: "Teacher", value: "Teacher" },
      { key: "Accountant", value: "Accountant" },
      { key: "Driver", value: "Driver" },
      { key: "Workers", value: "Workers" },
      { key: "General", value: "General" },
      { key: "Medical", value: "Medical" },
      { key: "Sales", value: "Sales" },
      { key: "Office", value: "Office" },
      { key: "Workers", value: "Workers" },
      { key: "Part Time", value: "Part Time" },
      { key: "Technicians", value: "Technicians" },
      { key: "Film", value: "Film" },
      { key: "Call Center", value: "Call Center" },
      { key: "Hotel", value: "Hotel" },
      { key: "Security", value: "Security" },
      { key: "Beauty", value: "Beauty" },
      { key: "Computer", value: "Computer" },
      { key: "HouseKeeping", value: "Housekeeping" },
    ]
  },
  {
    Services: [
      { key: "Astro", value: "Astro" },
      { key: "Mitrata", value: "Mitrata" },
      { key: "Tour and Travels", value: "Tour and Travels" },
      { key: "Immigration", value: "Immigration" },
      { key: "Trade Mark Regestration", value: "Trade Mark Regestration" },
      { key: "Entertainment", value: "Entertainment" },
      { key: "Cyber", value: "Cyber" },
      { key: "Packers & Movers", value: "Packers & Movers" },
      { key: "Solar Panel", value: "Solar Panel" },
      { key: "Pest Control", value: "Pest Control" },
    ]
  },
  {
    Sale: [
      { key: "Residential", value: "Residential" },
      { key: "Non-Residential", value: "Non-Residential" },
      { key: "Land", value: "Land" },
      { key: "Automobile", value: "Automobile" },
      { key: "Machinery", value: "Machinery" },
      { key: "Pets", value: "Pets" },
      { key: "Others", value: "Others" },
    ]
  },
  {
    Medical: [
      { key: "Sexual Consultation", value: "Sexual Consultation" },
      { key: "Psoraisis (Safed Daag)", value: "Psoraisis (Safed Daag)" },
      { key: "Hairfall", value: "Hairfall" },
      { key: "Beauty & Wellness", value: "Beauty & Wellness" },
      { key: "Magnetic Treatment", value: "Magnetic Treatment" },
      { key: "Nasha Churwayen", value: "Nasha Churwayen" },
      { key: "Bavasir/Fisher", value: "Bavasir/Fisher" },
      { key: "Lambai Badhayen", value: "Lambai Badhayen" },
    ]
  },
  {
    Business: [
      { key: "Loan/Finance", value: "Loan/Finance" },
      { key: "Mobile Tower", value: "Mobile Tower" },
      { key: "Massage", value: "Massage" },
      { key: "Beauty Parlour", value: "Beauty Parlour" },
      { key: "Small Scale industries", value: "Small Scale industries" },
      { key: "Cell Phones", value: "Cell Phones" },
      { key: "Inverter/Battery", value: "Inverter/Battery" },
      { key: "Computer/Printers", value: "Computer/Printers" },
    ]
  },
  {
    Education: [
      { key: "Academic Courses", value: "Academic Courses" },
      { key: "Coaching / Tution", value: "Coaching / Tution" },
      { key: "Hobby Classes", value: "Hobby Classes" },
      { key: "Uniforms", value: "Uniforms" },
      { key: "Motor Driving", value: "Motor Driving" },
      { key: "Others", value: "Others" },
    ]
  },
  {
    Information: [
      { key: "Personal Info.", value: "Personal Info." },
      { key: "Commercial Info", value: "Commercial Info" },
    ]
  },
  {
    Lease: [
      { key: "Residential", value: "Residential" },
      { key: "Non-Residential", value: "Non-Residential" },
    ]
  },
]

export const TreeDataBW = [
  {
    title: 'All Edition',
    value: 'All Edition',
    key: 'All Edition',
  },
 
  
  {
    title: 'North Power',
    value: 'North Power',
    key: 'North Power',
    children: [
      {
        title: 'New Delhi',
        value: 'New DelhiNOP',
        key: 'New DelhiNOP',
      },
      {
        title: 'Dharamshala',
        value: 'DharamshalaNOP',
        key: 'DharamshalaNOP',
      },
      {
        title: 'Rohtak',
        value: 'RohtakNOP',
        key: 'RohtakNOP',
      },
      {
        title: 'Hisar',
        value: 'HisarNOP',
        key: 'HisarNOP',
      },
      {
        title: 'Jalandhar',
        value: 'JalandharNOP',
        key: 'JalandharNOP',
      },
      {
        title: 'Chandigarh',
        value: 'ChandigarhNOP',
        key: 'ChandigarhNOP',
      },
     
      {
        title: 'Jammu',
        value: 'JammuNOP',
        key: 'JammuNOP',
      },
    ],

  },
  {
    title: 'New Delhi',
    value: 'New Delhi',
    key: 'New Delhi',
  },
  {
    title: 'Chandigarh',
    value: 'Chandigarh',
    key: 'Chandigarh',
  },
  {
    title: 'Himachal Pradesh',
    value: 'Himachal Pradesh',
    key: 'Himachal Pradesh',
    children: [

      {
        title: 'Dharamshala',
        value: 'Dharamshala',
        key: 'Dharamshala',
      },

    ],
  },
  {
    title: 'Haryana',
    value: 'Haryana',
    key: 'Haryana',
    children: [

      {
        title: 'Rohtak',
        value: 'Rohtak',
        key: 'Rohtak',
      },
      {
        title: 'Hisar',
        value: 'Hisar',
        key: 'Hisar',
      },

    ],
  },
  {
    title: 'Punjab',
    value: 'Punjab',
    key: 'Punjab',
    children: [

      {
        title: 'Jalandhar',
        value: 'Jalandhar',
        key: 'Jalandhar',
      },

    ],
  },
  
  {
    title: 'Jammu and Kashmir',
    value: 'Jammu and Kashmir',
    key: 'Jammu and Kashmir',
    children: [

      {
        title: 'Jammu',
        value: 'Jammu',
        key: 'Jammu',
      },

    ],
  },
  {
    title: 'Up + Uttrakhand',
    value: 'Up + Uttrakhand',
    key: 'Up + Uttrakhand',
    children: [
      
          {
            title: 'Kanpur',
            value: 'KanpurUPUK',
            key: 'KanpurUPUK',
          },
          {
            title: 'Lucknow',
            value: 'LucknowUPUK',
            key: 'LucknowUPUK',
          },
          {
            title: 'Jhansi',
            value: 'JhansiUPUK',
            key: 'JhansiUPUK',
          },
          {
            title: 'Gorakhpur',
            value: 'GorakhpurUPUK',
            key: 'GorakhpurUPUK',
          },
          {
            title: 'Varanasi',
            value: 'VaranasiUPUK',
            key: 'VaranasiUPUK',
          },
          {
            title: 'Allahabad',
            value: 'AllahabadUPUK',
            key: 'AllahabadUPUK',
          },
          {
            title: 'New Delhi',
            value: 'New DelhiUPUK',
            key: 'New DelhiUPUK',
          },
          {
            title: 'Agra',
            value: 'AgraUPUK',
            key: 'AgraUPUK',
          },

          {
            title: 'Meerut',
            value: 'MeerutUPUK',
            key: 'MeerutUPUK',
          },
          {
            title: 'Aligarh',
            value: 'AligarhUPUK',
            key: 'AligarhUPUK',
          },

          {
            title: 'Bareily',
            value: 'BareilyUPUK',
            key: 'BareilyUPUK',
          },
          {
            title: 'Moradabad',
            value: 'MoradabadUPUK',
            key: 'MoradabadUPUK',
          },
          {
            title: 'Dehradun',
            value: 'DehradunUPUK',
            key: 'DehradunUPUK',
          },
          {
            title: 'Nainital',
            value: 'NainitalUPUK',
            key: 'NainitalUPUK',
          },
        ],

  },
  {
    title: 'Uttarakhand',
    value: 'Uttarakhand',
    key: 'Uttarakhand',
    children: [

      {
        title: 'Dehradun',
        value: 'DehradunUK',
        key: 'DehradunUK',
      },
      {
        title: 'Nainital',
        value: 'NainitalUK',
        key: 'NainitalUK',
      },

    ],
  },
  {
    title: 'UttarPradesh',
    value: 'UttarPradesh',
    key: 'UttarPradesh',
    children: [
      {
        title: 'Kanpur',
        value: 'KanpurUP',
        key: 'KanpurUP',
      },
      {
        title: 'Lucknow',
        value: 'LucknowUP',
        key: 'LucknowUP',
      },
      {
        title: 'Jhansi',
        value: 'JhansiUP',
        key: 'JhansiUP',
      },
      {
        title: 'Gorakhpur',
        value: 'GorakhpurUP',
        key: 'GorakhpurUP',
      },
      {
        title: 'Varanasi',
        value: 'VaranasiUP',
        key: 'VaranasiUP',
      },
      {
        title: 'Allahabad',
        value: 'AllahabadUP',
        key: 'AllahabadUP',
      },
      {
        title: 'New Delhi',
        value: 'New DelhiUP',
        key: 'New DelhiUP',
      },
      {
        title: 'Agra',
        value: 'AgraUP',
        key: 'AgraUP',
      },

      {
        title: 'Meerut',
        value: 'MeerutUP',
        key: 'MeerutUP',
      },
      {
        title: 'Aligarh',
        value: 'AligarhUP',
        key: 'AligarhUP',
      },

      {
        title: 'Bareily',
        value: 'BareilyUP',
        key: 'BareilyUP',
      },
      {
        title: 'Moradabad',
        value: 'MoradabadUP',
        key: 'MoradabadUP',
      },

    ],
  },
  {
    title: "Central & East Up",
    value: "Central & East Up",
    key: "Central & East Up",
    children: [
      {
        title: 'Kanpur',
        value: 'KanpurCUP',
        key: 'KanpurCUP',
      },
      {
        title: 'Lucknow',
        value: 'LucknowCUP',
        key: 'LucknowCUP',
      },
      {
        title: 'Jhansi',
        value: 'JhansiCUP',
        key: 'JhansiCUP',
      },
      {
        title: 'Gorakhpur',
        value: 'GorakhpurCUP',
        key: 'GorakhpurCUP',
      },
      {
        title: 'Varanasi',
        value: 'VaranasiCUP',
        key: 'VaranasiCUP',
      },
      {
        title: 'Allahabad',
        value: 'AllahabadCUP',
        key: 'AllahabadCUP',
      },
    ]
  },
]
export const TreeDataC = [
  {
    title: 'All Edition',
    value: 'All Edition',
    key: 'All Edition',
  },
 
  {
    title: 'New Delhi',
    value: 'New Delhi',
    key: 'New Delhi',
  },
  {
    title: 'Rohtak',
    value: 'Rohtak',
    key: 'Rohtak',
  },
  {
    title: 'Hisar',
    value: 'Hisar',
    key: 'Hisar',
  },
  {
    title: 'Up + Uttrakhand',
    value: 'Up + Uttrakhand',
    key: 'Up + Uttrakhand',
    children: [
      {
        title: 'Kanpur',
        value: 'KanpurUPUK',
        key: 'KanpurUPUK',
      },
      {
        title: 'Lucknow',
        value: 'LucknowUPUK',
        key: 'LucknowUPUK',
      },
      {
        title: 'New Delhi',
        value: 'New DelhiUPUK',
        key: 'New DelhiUPUK',
      },
      {
        title: 'Agra',
        value: 'AgraUPUK',
        key: 'AgraUPUK',
      },

      {
        title: 'Meerut',
        value: 'MeerutUPUK',
        key: 'MeerutUPUK',
      },
    ]
  },
  {
    title: 'UttarPradesh',
    value: 'UttarPradesh',
    key: 'UttarPradesh',
    children: [
      {
        title: 'Kanpur',
        value: 'KanpurUP',
        key: 'KanpurUP',
      },
      {
        title: 'Lucknow',
        value: 'LucknowUP',
        key: 'LucknowUP',
      },
      {
        title: 'New Delhi',
        value: 'New DelhiUP',
        key: 'New DelhiUP',
      },
      {
        title: 'Agra',
        value: 'AgraUP',
        key: 'AgraUP',
      },

      {
        title: 'Meerut',
        value: 'MeerutUP',
        key: 'MeerutUP',
      },

    ],
  },

  {
    title: "Central & East Up",
    value: "Central & East Up",
    key: "Central & East Up",
    children: [
      {
        title: 'Kanpur',
        value: 'KanpurCUP',
        key: 'KanpurCUP',
      },
      {
        title: 'Lucknow',
        value: 'LucknowCUP',
        key: 'LucknowCUP',
      },
    ]
  },
]
export const TreeDataBWPersonal = [
 
 
  {
    title: 'New Delhi',
    value: 'New Delhi',
    key: 'New Delhi',
  },
  {
    title: 'Chandigarh',
    value: 'Chandigarh',
    key: 'Chandigarh',
  },
  {
    title: 'Dharamshala',
    value: 'Dharamshala',
    key: 'Dharamshala',
  },
  {
    title: 'Rohtak',
    value: 'Rohtak',
    key: 'Rohtak',
  },
  {
    title: 'Hisar',
    value: 'Hisar',
    key: 'Hisar',
  },
  {
    title: 'Jalandhar',
    value: 'Jalandhar',
    key: 'Jalandhar',
  },
  {
    title: 'Jammu',
    value: 'Jammu',
    key: 'Jammu',
  },
  {
    title: 'Kanpur',
    value: 'Kanpur',
    key: 'Kanpur',
  },
  {
    title: 'Lucknow',
    value: 'Lucknow',
    key: 'Lucknow',
  },
  {
    title: 'Jhansi',
    value: 'Jhansi',
    key: 'Jhansi',
  },
  {
    title: 'Gorakhpur',
    value: 'Gorakhpur',
    key: 'Gorakhpur',
  },
  {
    title: 'Varanasi',
    value: 'Varanasi',
    key: 'Varanasi',
  },
  {
    title: 'Allahabad',
    value: 'Allahabad',
    key: 'Allahabad',
  },
  {
    title: 'Agra',
    value: 'Agra',
    key: 'Agra',
  },

  {
    title: 'Meerut',
    value: 'Meerut',
    key: 'Meerut',
  },
  {
    title: 'Aligarh',
    value: 'Aligarh',
    key: 'Aligarh',
  },

  {
    title: 'Bareily',
    value: 'Bareily',
    key: 'Bareily',
  },
  {
    title: 'Moradabad',
    value: 'Moradabad',
    key: 'Moradabad',
  },
  {
    title: 'Dehradun',
    value: 'Dehradun',
    key: 'Dehradun',
  },
  {
    title: 'Nainital',
    value: 'Nainital',
    key: 'Nainital',
  },
]
export const TreeDataCPersonal = [
 
 
  {
    title: 'New Delhi',
    value: 'New Delhi',
    key: 'New Delhi',
  },
  {
    title: 'Rohtak',
    value: 'Rohtak',
    key: 'Rohtak',
  },
  {
    title: 'Hisar',
    value: 'Hisar',
    key: 'Hisar',
  },
  {
    title: 'Kanpur',
    value: 'Kanpur',
    key: 'Kanpur',
  },
  {
    title: 'Lucknow',
    value: 'Lucknow',
    key: 'Lucknow',
  },
  {
    title: 'Agra',
    value: 'Agra',
    key: 'Agra',
  },

  {
    title: 'Meerut',
    value: 'Meerut',
    key: 'Meerut',
  },
]

export const PriceTreeData = {
  "All Edition": {
    Display: { color: "2644", bw: "2466" },
    Row: { color: "7533", bw: "6962" },
    Extra: { color: "220", bw: "300" },
  },
  "UttarPradesh": {
    Display: { color: "2160", bw: "1997" },
    Row: { color: "6025", bw: "5249" },
    Extra: { color: "82", bw: "75" },
  },
  "Uttarakhand": {
    Display: { color: "0", bw: "398" },
    Row: { color: "0", bw: "1202" },
    Extra: { color: "0", bw: "45" }
  },
  "Jammu and Kashmir": {
    Display: { color: "0", bw: "131" },
    Row: { color: "0", bw: "245" },
    Extra: { color: "0", bw: "30" }
  },
  "Himachal Pradesh": {
    Display: { color: "0", bw: "313" },
    Row: { color: "0", bw: "812" },
    Extra: { color: "0", bw: "30" }
  },
  "Punjab": {
    Display: { color: "0", bw: "157" },
    Row: { color: "0", bw: "560" },
    Extra: { color: "0", bw: "30" }
  },
  "Haryana": {
    Display: { color: "0", bw: "412" },
    Row: { color: "0", bw: "551" },
    Extra: { color: "0", bw: "45" }
  },
  "Up + Uttrakhand": {
    Display: { color: "2315", bw: "2140" },
    Row: { color: "6728", bw: "6218" },
    Extra: { color: "165", bw: "150" }
  },
  "Central & East Up": {
    Display: { color: "942", bw: "870" },
    Row: { color: "2354", bw: "2175" },
    Extra: { color: "110", bw: "100" }
  },
  "North Power": {
    Display: { color: "0", bw: "697" },
    Row: { color: "0", bw: "1886" },
    Extra: { color: "0", bw: "45" }
  },

  "Agra": {
    Display: { color: "439", bw: "381" },
    Row: { color: "1415", bw: "1230" },
    Extra: { color: "40", bw: "35" },
    PersonalRow: { color: "790", bw: "719" }
  },
  "Meerut": {
    Display: { color: "345", bw: "300" },
    Row: { color: "1131", bw: "984" },
    Extra: { color: "46", bw: "40" },
    PersonalRow: { color: "698", bw: "634" }
  },
  "Bareily": {
    Display: { color: "0", bw: "259" },
    Row: { color: "0", bw: "942" },
    Extra: { color: "35", bw: "30" },
    PersonalRow: { color: "0", bw: "634" }
  },
  "Moradabad": {
    Display: { color: "0", bw: "232" },
    Row: { color: "0", bw: "957" },
    Extra: { color: "0", bw: "30" },
    PersonalRow: { color: "0", bw: "634" }
  },
  "Aligarh": {
    Display: { color: "0", bw: "142" },
    Row: { color: "0", bw: "629" },
    Extra: { color: "0", bw: "25" },
    PersonalRow: { color: "0", bw: "272" }
  },
  "Kanpur": {
    Display: { color: "354", bw: "308" },
    Row: { color: "1418", bw: "1233" },
    Extra: { color: "46", bw: "40" },
    PersonalRow: { color: "541", bw: "492" }
  },
  "Lucknow": {
    Display: { color: "532", bw: "463" },
    Row: { color: "1161", bw: "1010" },
    Extra: { color: "46", bw: "40" },
    PersonalRow: { color: "606", bw: "550" }
  },
  "Jhansi": {
    Display: { color: "0", bw: "151" },
    Row: { color: "0", bw: "271" },
    Extra: { color: "0", bw: "15" },
    PersonalRow: { color: "0", bw: "175" }

  },
  "Varanasi": {
    Display: { color: "0", bw: "364" },
    Row: { color: "0", bw: "842" },
    Extra: { color: "0", bw: "40" },
    PersonalRow: { color: "0", bw: "453" }

  },
  "Allahabad": {
    Display: { color: "0", bw: "280" },
    Row: { color: "0", bw: "897" },
    Extra: { color: "0", bw: "25" },
    PersonalRow: { color: "0", bw: "266" }
  },
  "Gorakhpur": {
    Display: { color: "0", bw: "195" },
    Row: { color: "0", bw: "645" },
    Extra: { color: "0", bw: "25" },
    PersonalRow: { color: "0", bw: "285" }
  },
  "Dehradun": {
    Display: { color: "0", bw: "273" },
    Row: { color: "0", bw: "957" },
    Extra: { color: "0", bw: "30" },
    PersonalRow: { color: "0", bw: "544" }
  },
  "Nainital": {
    Display: { color: "0", bw: "218" },
    Row: { color: "0", bw: "711" },
    Extra: { color: "0", bw: "25" },
    PersonalRow: { color: "0", bw: "364" }
  },
  "New Delhi": {
    Display: { color: "454", bw: "412" },
    Row: { color: "1210", bw: "1052" },
    Extra: { color: "35", bw: "30" },
    PersonalRow: { color: "641", bw: "583" }
  },
  "Chandigarh": {
    Display: { color: "0", bw: "157" },
    Row: { color: "0", bw: "341" },
    Extra: { color: "0", bw: "30" },
    PersonalRow: { color: "0", bw: "265" }
  },
  "Jalandhar": {
    Display: { color: "0", bw: "157" },
    Row: { color: "0", bw: "560" },
    Extra: { color: "0", bw: "30" },
    PersonalRow: { color: "0", bw: "266" }
  },
  "Jammu": {
    Display: { color: "0", bw: "131" },
    Row: { color: "0", bw: "245" },
    Extra: { color: "0", bw: "30" },
    PersonalRow: { color: "0", bw: "259" }
  },
  "Dharamshala": {
    Display: { color: "0", bw: "151" },
    Row: { color: "0", bw: "505" },
    Extra: { color: "0", bw: "30" },
    PersonalRow: { color: "0", bw: "285" }
  },
  "Rohtak": {
    Display: { color: "175", bw: "125" },
    Row: { color: "350", bw: "300" },
    Extra: { color: "35", bw: "30" },
    PersonalRow: { color: "300", bw: "275" }
  },
  "Hisar": {
    Display: { color: "125", bw: "100" },
    Row: { color: "150", bw: "100" },
    Extra: { color: "35", bw: "30" },
    PersonalRow: { color: "100", bw: "90" }
  }
}

