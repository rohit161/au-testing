import React, { useState, useCallback, useEffect } from "react";
import { Box, Grid, Paper, TextField, MenuItem } from "@material-ui/core";
import { styles } from "../common/styles";
import {
  renderButton,
} from "../common/DisplayComponent";
import RowAdsType from "../adsType/row";
import DisplayAdsType from "../adsType/display";
import DoubleDisplayAdsType from "../adsType/doubledisplay";
import { Calendar, DateObject } from "react-multi-date-picker"
import FormControlLabel from '@mui/material/FormControlLabel';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';

const FontWeight = {
  fontWeight: "500",
  margin: "10px 0"
}

const Step2 = ({ state, handleChange, handleNext, handlePrev, data, imageHandler }) => {
  const [value, setValue] = useState(new DateObject([]));
  const [disabled, setDisabled] = useState(false)
  const [errorMsg, setErrorMsg] = useState(false)
  const [dateRangeMsg, setDateRangeMsg] = useState("")
  const [datedisabled, setDateDisabled] = useState(true)
  const [expLength, setExpLength] = useState(0)
  const [minDate, setMinDate] = useState("")
  const [maxDate, setMaxDate] = useState("")

  const DateDisabledFunc = () => {
    if (data.adsType == "Row") {
      if (data.scheme && data.description) {
        setDateDisabled(false);
      } else {
        setDateDisabled(true);
      }
    } else {
      if (data.height && data.imgName && data.scheme) {
        setDateDisabled(false);
      } else {
        setDateDisabled(true)
      }
    }
  }
  const SchemeDateValidate = () => {
    if (data.scheme) {
      if (data.scheme == "Single") {
        setExpLength(1); setDateRangeMsg("");
        if (data.date.length == expLength) {
          setDisabled(false); setErrorMsg("")
        } else {
          setErrorMsg("Please select only 1 date"); setDisabled(true)
        }
      } else if (data.scheme == "2 paid + 1 free") {
        setExpLength(3); setDateRangeMsg("*The dates must be within 21 days");
        if (data.date.length == expLength) {
          setDisabled(false); setErrorMsg("")
        } else {
          setErrorMsg("Please select only 3 dates"); setDisabled(true)
        }
      } else if (data.scheme == "5 paid + 3 free") {
        setExpLength(8); setDateRangeMsg("*The dates must be within 42 days");
        if (data.date.length == expLength) {
          setDisabled(false); setErrorMsg("")
        } else {
          setErrorMsg("Please select only 8 dates"); setDisabled(true)
        }
      } else if (data.scheme == "15 paid + 15 free") {
        setExpLength(30); setDateRangeMsg("*The dates must be within 90 days");
        if (data.date.length == expLength) {
          setDisabled(false); setErrorMsg("")
        } else {
          setErrorMsg("Please select only 30 dates"); setDisabled(true)
        }
      }
    }

  }
  const ButtonDisabledCD = () => {
    if (data.height && data.width && data.date.length > 0 && data.imgUrl) {
      return setDisabled(false)
    } else {
      return setDisabled(true)
    }
  }
  const ButtonDisabledRow = () => {
    if (data.title && data.description && data.date.length > 0 && data.scheme) {
      if (data.description) {
        if (data.description.match(/(\w+)/g).length > 25) {
          console.log("Desciption value false")
          return setDisabled(false); 
        }else{
          return setDisabled(true)
        }
      }else{
        return setDisabled(false)
      }
    } else {
      return setDisabled(true)
    }
  }
 
  useEffect(() => {
    data["date"] = value;
    DateDisabledFunc();
    SchemeDateValidate();
    if (data.adsType == "Row") {
      ButtonDisabledRow()
    } else {
      ButtonDisabledCD()
    }
  })
  useEffect(() => {
    if (data.date) {
      MaxDateFunc();
    }
  }, [value,data.scheme])

  const MaxDateFunc = () => {
    if (value[0]) {
      if (data.scheme == "2 paid + 1 free") {
        if(data.categoryName == "Matrimonial"){
          if (new Date(value[0]) > new Date(Date.now() + 92 * 86400000 - 28 * 86400000)) {
            setMinDate(new Date(Date.now() + 92 * 86400000 - 28 * 86400000));
            setMaxDate(new Date(Date.now() + 92 * 86400000));
          } else {
            setMaxDate(new Date(value[0] + 28 * 86400000));
            setMinDate(new Date(value[0]))
          }
        }else{
          if (new Date(value[0]) > new Date(Date.now() + 92 * 86400000 - 20 * 86400000)) {
            setMinDate(new Date(Date.now() + 92 * 86400000 - 20 * 86400000));
            setMaxDate(new Date(Date.now() + 92 * 86400000));
          } else {
            setMaxDate(new Date(value[0] + 20 * 86400000));
            setMinDate(new Date(value[0]))
          }
        }
      } else if (data.scheme == "5 paid + 3 free") {
        if (new Date(value[0]) > new Date(Date.now() + 92 * 86400000 - 41 * 86400000)) {
          setMinDate(new Date(Date.now() + 92 * 86400000 - 41 * 86400000));
          setMaxDate(new Date(Date.now() + 92 * 86400000));
        } else {
          setMaxDate(new Date(value[0] + 41 * 86400000));
          setMinDate(new Date(value[0]))
        }
      } else if (data.scheme == "15 paid + 15 free") {
        if (new Date(value[0]) > new Date(Date.now() + 92 * 86400000 - 89 * 86400000)) {
          setMinDate(new Date(Date.now() + 92 * 86400000 - 89 * 86400000));
          setMaxDate(new Date(Date.now() + 92 * 86400000));
        } else {
          setMaxDate(new Date(value[0] + 89 * 86400000));
          setMinDate(new Date(value[0]))
        }
      } else {
        setMaxDate(new Date(Date.now() + 92 * 86400000));
        setMinDate(new Date(Date.now() + 2 * 86400000));
      }
    }else {
      setMaxDate(new Date(Date.now() + 92 * 86400000));
      setMinDate(new Date(Date.now() + 2 * 86400000));
    }
  }

   
    return (
      <Paper elevation={0} style={styles.steps}>
        <Grid container component={Box} mt={2} p={2} style={{ background: "#FFDB58", ...FontWeight, borderRadius: "8px 8px 0px 0px" }}>
          <Grid>Compose Your Ad </Grid>
        </Grid>
        {/* Row Ad Component */}
        {data.adsType == "Row" ? <RowAdsType state={state} handleChange={handleChange} handlePrev={handlePrev} data={data} /> : null}
        {/* Display Ad Component */}
        {data.adsType == "Classified Display" || data.adsType == "Double Column Classified Display" ?
          <DisplayAdsType state={state} handleChange={handleChange} imageHandler={imageHandler} handlePrev={handlePrev} data={data} />
          : null}
        {/* Double Display Ad Component */}
        {/* {data.adsType == "Double Column Classified Display" ?
        <DoubleDisplayAdsType state={state} handleChange={handleChange} imageHandler={imageHandler} handlePrev={handlePrev} data={data} />
        : null} */}
        {/* Bottom Date Component */}
        <Grid container component={Box} mt={2} p={2} style={{ background: "#FFDB58", ...FontWeight, borderRadius: "8px 8px 0px 0px" }}>
          <Grid>When to Publish </Grid>
        </Grid>
        <Grid container component={Box} justify='space-between' style={{ textAlign: "center", margin: "20px 0" }}>
          <Grid xs={12} sm={5}>
            <Calendar
              multiple
              disabled={datedisabled}
              style={datedisabled ? { color: "#ccc" } : null}
              minDate={minDate}
              maxDate={maxDate}
              value={value}
              onChange={setValue}
              // hideWeekDays={true}
              mapDays={({ date }) => {
                let isWeekend = [1, 2, 3, 4, 5, 6].includes(date.weekDay.index)
                if (isWeekend && data.category[0].name == "Matrimonial")
                  return {
                    disabled: true,
                    style: { color: "#ccc" },
                    onClick: () => alert("WeekDays is not available for Matrimonial Ads")
                  }
              }}
            />
          </Grid>
          <Grid xs={12} sm={5} lg={6} style={{ textAlign: "left" }}>
            <p style={FontWeight}>Your Ads will be published in these dates.</p>
            <p style={{ marginBottom: "10px" }}>Applicable Schemes on Classified ROW and CD:</p>
            {data.scheme ? <p style={{ marginBottom: "10px" }}>{data.categoryName == "Matrimonial" && data.scheme == "2 paid + 1 free" ? "*The dates must be within 29 days" : dateRangeMsg}</p> : null}


            {data.categoryName == "Matrimonial" ?
              <>
                <RadioGroup onChange={handleChange} value={data['scheme']} id="adsType" name="scheme" row aria-label="scheme" >
                  <Grid xs={12}>
                    <FormControlLabel className="adstype-label step3-label" size={"medium"} value="Single" control={<Radio />} label={"Single Ad"} />
                  </Grid>
                  <Grid xs={12}>
                    <FormControlLabel className="adstype-label step3-label" size={"medium"} value="2 paid + 1 free" control={<Radio />} label={"Buy 2 Ads and Get 1 Free"} />
                  </Grid>
                </RadioGroup>
              </>
              :
              <>
                <RadioGroup onChange={handleChange} value={data['scheme']} id="adsType" name="scheme" row aria-label="scheme" >
                  <Grid xs={12}>
                    <FormControlLabel className="adstype-label step3-label" size={"medium"} value="Single" control={<Radio />} label={"Single Ad"} />
                  </Grid>
                  <Grid xs={12}>
                    <FormControlLabel className="adstype-label step3-label" size={"medium"} value="2 paid + 1 free" control={<Radio />} label={"Buy 2 Ads and Get 1 Free"} />
                  </Grid>
                  <Grid xs={12}>
                    <FormControlLabel className="adstype-label step3-label" size={"medium"} value="5 paid + 3 free" control={<Radio />} label={"Buy 5 Ads and Get 3 Free"} />
                  </Grid>
                  <Grid xs={12}>
                    <FormControlLabel className="adstype-label step3-label" size={"medium"} value="15 paid + 15 free" control={<Radio />} label={"Buy 15 Ads and Get 15 Free"} />
                  </Grid>
                </RadioGroup>
              </>
            }
            <Grid container mt={2}>
              {value.length > 0 ?
                value.map(x => {
                  return <Box style={DateBoxStyle}>{x.format("DD/MM/YYYY")}<span class="date-display" ></span></Box>
                }) :
                null
              }
              {errorMsg.length > 5 ?
                <p style={{ color: "rgb(209, 32, 35)", width: "100%", marginTop: "8px" }}>*{errorMsg}</p>
                : null}
            </Grid>
          </Grid>
        </Grid>
        <Grid container component={Box} justify='space-between' mt={2} p={2}>
          <Box >
            {renderButton({
              label: "Back",
              color: "default",
              onClick: handlePrev,
            })}
          </Box>
          <Box ml={2}>{renderButton({ label: "Bill Summary", onClick: handleNext, variant: "contained", background: "#D12023", disabled: disabled })}</Box>
        </Grid>
      </Paper>
    );
  };

  export default Step2;

  const DateBoxStyle = {
    padding: "10px", background: "#262626",
    margin: "10px 5px 0 0", width: "max-content", color: "#FFDB58", borderRadius: "8px",
    display: "flex",
    alignItems: "center"
  }

  const PremiumStyle = { position: "absolute", right: "5px", top: "5px" }