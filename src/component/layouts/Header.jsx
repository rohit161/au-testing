import React from 'react'
import logo from "../../images/CLASSIFIED-Logo.png";
import { Grid } from "@material-ui/core";

const Header = () => {
    return (
        <div style={{ padding: "10px",  background: "#FFDB58" , position:"fixed", width:"100%",top:"0",zIndex:"5" }}>
            <Grid item xs={12} sm={6} lg={5} className="logo-grid">
               <a style={{display:"flex",flexWrap:"wrap",alignItems:"center",justifyContent:"center"}} href="https://wordpress-466301-2255911.cloudwaysapps.com/"> <img className="amarujalalogo" src={logo} alt="" style={{padding:"5px"}}/><h3 style={{fontWeight:"400"}}>CLASSIFIED</h3></a>
            </Grid>
        </div>
    )
}

export default Header
