import React from 'react'
import { Grid } from "@material-ui/core";

const Footer = () => {
    return (
        <Grid container item xs={12} sm={12} lg={12} >
            <Grid item xs={12} sm={12} lg={12}  style={{ background: "#D12023", color: "white", textAlign: "center", padding:"10px 20px" }}>
                <p> If you have any question, comment or concern, please call the Classified </p>
                <p> Advertising Department at <br/> + 91-7617499777</p> 
                <h3 style={{color:"white"}}> Monday - Saturday Time : 10.00 AM to 6.00 PM </h3>
            </Grid>
            <Grid item xs={12} sm={12} lg={12} style={{ textAlign: "center" }}>
                <ul className=" footer-list ">

                    <li> <a target="_blank " href="https://wordpress-466301-2255911.cloudwaysapps.com/"> HOME <span className="opacity-step3 ml-10"> | </span> </a>
                    </li>

                    <li> <a target="_blank " href="https://wordpress-466301-2255911.cloudwaysapps.com/about/"> ABOUT US <span className="opacity-step3 ml-10"> | </span> </a>
                    </li>

                    <li> <a target="_blank " href="https://easyconnect.amarujala.com/Classifiedonline/terms_and_conditions.html"> TERMS AND CONDITIONS <span className="opacity-step3 ml-10"> | </span> </a>
                    </li>

                    <li> <a target="_blank" href="https://easyconnect.amarujala.com/Classifiedonline/privacy_policy.html"> PRIVACY POLICY <span className="opacity-step3 ml-10">| </span> </a>
                    </li>

                    <li> <a target="_ blank" href="https://epaper.amarujala.com/delhi-city/20211223/01.html?format=img&ed_code= delhi-city"> E-PAPER <span className="opacity-step3 ml-10"> | </span> </a>
                    </li>

                    <li> <a target="_blank " href="https://wordpress-466301-2255911.cloudwaysapps.com/contact-us/"> CONTACT US <span className="opacity-step3 ml-10"> | </span> </a>
                    </li>

                    <li> <a target="_blank " href="https://easyconnect.amarujala.com/Classifiedonline/FAQ.aspx"> FAQ <span className="opacity-step3 ml-10"> | </span> </a>
                    </li>


                    < li> <a target="_blank" href="https://easyconnect.amarujala.com/Classifiedonline/uc/Images/Final_Rate_Card-2018-19.pdf"> RATE CARD </a>
                    </li>
                </ul>
            </Grid>
        </Grid>
    )
}

export default Footer
