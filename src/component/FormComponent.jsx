import React, { Component, useState } from "react";
import PropTypes from "prop-types";
import {
  Button,
  Box,
  Grid,
  Paper,
  withStyles,
  Stepper,
  Step,
  StepLabel,
} from "@material-ui/core";
import Step1 from "./Steps/step1";
import Step2 from "./Steps/step2";
import Step3 from "./Steps/step3";
import FinalStep from "./Steps/FinalStep";
import { styles } from "./common/styles";
import Header from "./layouts/Header";
import Footer from "./layouts/Footer";



class FormComponent extends Component {
  state = {
    data: {
      name: "",
      email: "",
      mobile: "",
      address: "",
      gst: "",
      categoryName: "",
      division: "",
      subDivision: "",
      color: "",
      adsType: "",
      stateName: "",
      city: "",
      title: "",
      description: "",
      filename: "",
      imgName: "",
      imgUrl: "",
      idProofName: "",
      idProof: "",
      Bold: "",
      Border: "",
      height: "4",
      width: "3",
      eyeCatcher: "",
      states: "",
      category: "",
      scheme: "",
      price: "",
      edition: "",
    },
    errors: {},
    steps: [
      { label: "Target", desc: "Choose Your Ad Type" },
      { label: "Compose ", desc: "Configure your ad look" },
      { label: "Payment", desc: "Confirmation" },
    ],
    stepCount: 0,
  };
  render() {
    const { classes } = this.props;
    function encode(data) {
      return Object.keys(data)
        .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
        .join("&")
    }

    const handleSubmit = (e) => {
      e.preventDefault();
      e.preventDefault()
      fetch("https://wordpress-466301-2255911.cloudwaysapps.com/wp-json/contact-form-7/v1/contact-forms/1017/feedback", {
        method: "POST",
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        body: encode({ ...this.state.data })
      }).then(() => {  window.scrollTo(0, 0);handleNextStep() }).catch(error => alert(error))

    };
    const handleOnChange = ({ target }) => {
      const { data, errors } = this.state;
      // console.log("target",target)
      data[target.name] = target.value;
      if (target.name == "Bold" || target.name == "Border" || target.name == "BillCompanyName") {
        data[target.name] = target.checked;
      } else if (target.name == "category") {
        data["division"] = "";
        data["subDivision"] = "";
        data['color'] = "";
        data['edition'] = [];
        data['price'] = "";
      } else if (target.name == "height") {
        if (data.height > 25) {
          data["height"] = ""
        } else {
          data["height"] = data.height
        }
      }else if (target.name == "adsType"){
        data['color'] = "";
      }
      this.setState({ data, errors });
    };
    const imageHandler = (e) => {
      const { data, errors } = this.state;
      console.log("eventTarget", e)
      const reader = new FileReader();
      reader.onload = () => {
        if (reader.readyState === 2) {
            data["imgUrl"] = reader.result;
            data["imgName"] = e.target.files[0].name
          this.setState({ data, errors });
        }
      }

      reader.readAsDataURL(e.target.files[0]);
      this.setState({ data, errors });
    };

    const handleNextStep = () => {
      let { stepCount, data } = this.state;
      // console.log("stepCount", stepCount);
      stepCount = stepCount + 1;
      this.setState({ stepCount });
      window.scrollTo(0, 0);
    };
    const handleBackStep = () => {
      let { stepCount, data } = this.state;
      stepCount = stepCount - 1;
      if (stepCount == 0) {
        data = {}
      }
      this.setState({ stepCount, data });
    };
    const DocHandler = (e) => {
      let { data } = this.state;
      let DocArr = []
      let DocArrName = []
      var files = e.target.files;
      if (files.length) {
        for (let index = 0; index < files.length; index++) {
          //instantiate a FileReader for the current file to read
          const reader = new FileReader();
          reader.onload = () => {
            if (reader.readyState === 2) {
              console.log("Results", e.target.files[index].name)
              DocArr.push(reader.result)
              DocArrName.push(e.target.files[index].name)
            }
          }
          reader.readAsDataURL(files[index]);
        }
        data["idProofURL"] = DocArr;
        data["idProofName"] = DocArrName;
      }
      this.setState({ data });
    }

    console.log("stepCount", this.state.stepCount)

    const getStepContent = (step) => {
      switch (step) {
        case 0:
          return (
            <Step1
              state={this.state}
              handleChange={handleOnChange}
              handleNext={handleNextStep}
              data={this.state.data}
            />

          );
        case 1:
          return (
            <Step2
              state={this.state}
              handleChange={handleOnChange}
              handleNext={handleNextStep}
              handlePrev={handleBackStep}
              data={this.state.data}
              imageHandler={imageHandler}
            // dateState={this.setState({date})}
            />
          );
        case 2:
          return (
            <Step3
              state={this.state}
              handleChange={handleOnChange}
              handleNext={handleNextStep}
              handlePrev={handleBackStep}
              handleSubmit={handleSubmit}
              data={this.state.data}
              DocHandler={DocHandler}
            />
          );
        case 3:
          return <FinalStep data={this.state.data} />;
        default:
          return (
            <Step1
              state={this.state}
              handleChange={handleOnChange}
              handleNext={handleNextStep}

            />
          );
      }
    };

    return (
      <>
        <Header />
        <Grid container className={classes.formContainer}>
          <Grid item xs={12} sm={12} lg={9}>
            <form enctype='multipart/form-data' onSubmit={handleSubmit} method="POST" id="bookForm" className={classes.form}>
              <Paper elevation={0} component={Box} mb={1} >
                <Stepper activeStep={this.state.stepCount} className="paper-label" alternativeLabel>
                  {this.state.steps.map((item) => (
                    <Step key={item.label}>
                      <StepLabel className={classes.formsteplabel}>{item.label}<br /><span className={classes.formsteplabel + " step-label"} >{item.desc}</span></StepLabel>
                    </Step>
                  ))}
                </Stepper>
              </Paper>
              {getStepContent(this.state.stepCount)}
            </form>
          </Grid>
        </Grid>
        <Footer />
      </>
    );
  }
}

FormComponent.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FormComponent);
