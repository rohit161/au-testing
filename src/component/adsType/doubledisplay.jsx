import React from "react";
import { Box, Grid, TextField } from "@material-ui/core";
import {
    renderInputField,
} from "../common/DisplayComponent";
import profile from "../../images/smallDemo.jpg";

const FontWeight = {
    fontWeight: "500",
    margin: "10px 0"
}
const DisplayAdsType = ({ state, handleChange, data, imageHandler }) => {


    return (

        <Grid container component={Box} className="grid-text-center grid-text-left mob-col-reverse" justify='space-between' style={{ marginBottom: "25px" }}>
            <Grid xs={12} sm={5} lg={5}>
                <h2 style={FontWeight}>Input</h2>
                <h3 style={{ float: "left" }}>Add Attachment</h3>
                <Grid>
                    <TextField
                        label={"Img/PDF"}
                        variant='outlined'
                        color='primary'
                        size='small'
                        margin={"normal"}
                        fullWidth={true}
                        name={"imgName"}
                        value={data["imgName"]}
                        onChange={handleChange}
                        disabled
                    />
                </Grid>
                <Grid container style={{ justifyContent: "space-between", alignItems: "center" }}>
                    <p style={{ display: "block", width: "100%", textAlign: "left" }}>Note : Attachment must be in PDF/JPEG only</p>
                    <Grid>
                        <input type="file" accept="application/Image,application/pdf" name="image-upload" id="input" onChange={imageHandler} />
                        <div className="label border-radius-8" style={{ padding: "10px", background: "#000", color: "white", cursor:"pointer"  }}>
                            <label className="image-upload" htmlFor="input" >
                                Choose File
                            </label>
                        </div>
                    </Grid>
                </Grid>

                <Grid>
                    {renderInputField({
                        state,
                        name: "height",
                        label: "Height in cm",
                        onChange: handleChange,
                        type: "number",
                        helperText: "Min - 4cm and Max - 40cm"
                    })}
                </Grid>
                <Grid>
                    {renderInputField({
                        state,
                        name: "width",
                        label: "Width in cm",
                        onChange: handleChange,
                        type: "number",
                        disabled: true
                    })}

                </Grid>
            </Grid>
            <Grid xs={12} sm={5} lg={5} >
                <h2 style={FontWeight}>Preview</h2>
                {/* <Grid container justify="space-around" mt={2}>
                    <Grid xs={12} sm={5} lg={5} style={ImgGrid}>
                        <div className="img-holder">
                            {data.imgName.includes(".pdf") ?
                                <iframe src={data.imgUrl} style={{ height: "235px", width: "100%" }}></iframe> :
                                <img src={data.imgUrl ? data.imgUrl : profile} alt="" id="img" className="img" />
                            }
                        </div>
                    </Grid>
                    <Grid className="xs-display-none" xs={0} sm={5} lg={5} style={ImgGrid}>
                        <div className="img-holder">
                            {data.imgName.includes(".pdf") ?
                                <iframe src={data.imgUrl} style={{ height: "235px", width: "100%" }}></iframe> :
                                <img src={data.imgUrl ? data.imgUrl : profile} alt="" id="img" className="img" />
                            }
                        </div>
                    </Grid>
                </Grid> */}
                <div className="img-holder">
                    {data.imgName ? data.imgName.includes(".pdf") ?
                        <iframe src={data.imgUrl} style={{ height: "320px", width: "300px" }}></iframe> :
                        <img src={data.imgUrl ? data.imgUrl : profile} alt="" id="img" className="img" />
                        : <img src={ profile} alt="" id="img" className="img" />}
                </div>
            </Grid>
        </Grid >
    );
};

export default DisplayAdsType;

const ImgGrid = { padding: "10px 0", borderRadius: "8px", marginTop: "15px", }