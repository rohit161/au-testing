import React, { useEffect, useState } from "react";
import { Box, Grid, Button, TextField, MenuItem } from "@material-ui/core";
import {
  renderSelect,
  renderInputField,
} from "../common/DisplayComponent";
import Tooltip from '@mui/material/Tooltip';
import HelpIcon from '@mui/icons-material/Help';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import premiumImg from "../../images/premium-img.svg"
import TickImg from "../../images/tick.png"
import HeartImg from "../../images/heart.png"
import axios from 'axios';
import _ from 'lodash'

const FontWeight = {
  fontWeight: "500",
  margin: "10px 0"
}

const RowAdsType = ({ state, handleChange, data }) => {
  const [transDemo, setTransDemo] = useState('')
  const translateText = () => {
    let textTranslate = {
      q: data.description,
      source: 'en',
      target: 'hi'
    }
    const options = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': `Bearer ${"APIKey"}` } ,
      data: textTranslate,
      url:"https://translate.mentality.rip/translate"
    };
    axios(options)
      .then((response) => {
        console.log("text", response.data.translatedText)
        data['description'] = response.data.translatedText;
        setTransDemo(response.data.translatedText)
      })
  }
  useEffect(() => {
    if (data.title == "") {
      data["title"] = data.division
    }
  })
  useEffect(() => {
    if (data.description) {
      data["wordCount"] = data.description.match(/(\w+)/g).length 
    }else{
      data["wordCount"] = 0
    }
  })
  // console.log("demo text", data)
  const TitleStyle = (data) => {
    return {
      minHeight: "22px", background: "#A0A0A0",
      padding: "10px 0", color: "white",
      borderRadius: "8px 8px 0 0", textTransform: "uppercase",
      fontWeight: `${data.Bold ? "bold" : "normal"}`,
      border: `${data.Border ? "2px solid black" : "none"}`,
      textAlign: "center"
    }
  }

  const DescStyle = (data) => {
    return {
      background: "#f9f9f9", padding: "10px 0",
      minHeight: "250px", borderRadius: "0px 0px 8px 8px",
      padding: "15px",
      fontWeight: `${data.Bold ? "bold" : "normal"}`,
      border: `${data.Border ? "2px solid black" : "none"}`,
      alignItems: "flex-start",
      wordBreak: "break-word",
      display: "flex"
    }
  }

  const EyeCatcherData = [
    { key: "--------", value: "select" },
    { key: "Tick Mark", value: "tick-mark" },
    { key: "Heart", value: "heart" }
  ]

  const NumberwithComma = (x) => {
    // return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",")
    return x.toLocaleString();
  }

  return (
    <Grid container className="grid-text-center grid-text-left mob-col-reverse" component={Box} justify='space-between' style={{ marginBottom: "25px" }}>

      <Grid xs={12} sm={5}>
        <h2 style={FontWeight}>Input</h2>
        <Grid>
          {renderInputField({
            state,
            name: "title",
            label: "Title",
            onChange: handleChange,
            value: data.title ? data.title : data.division
          })}
          {renderInputField({
            state,
            name: "description",
            label: "Description (min-25 words, max-50 words)",
            rows: "10",
            onChange: handleChange,
          })}
           {data.description ?  <p style={{color:"rgb(209, 32, 35)",textAlign:"left"}}>{data.description.match(/(\w+)/g).length <25 ? "*Please have at least minimum 25 words": data.description.match(/(\w+)/g).length > 50 ?"*Extra Charge Applies for above 50 words ":null}</p>:null}
        </Grid>
        <Grid container justify="space-between" style={{ alignItems: "flex-end",marginTop:"10px" }}>
         <div><b>Select Ads-On</b></div>
          <span style={{ fontSize: "15px" }}> Note: <img src="/premium-big.png" /> This is a premium feature
          </span>
        </Grid>
        <Grid container column-spacing={2} xs={12} style={{ marginTop: "20px" }}>
          <Grid>
            <Tooltip title={<>35% Premium</>} placement="top">
              <Box mr={2} mb={2} className="position-relative">
                <FormGroup className="borderButton" style={data.Bold ? { ...BorderButton, background: "#ffdb58", border: "3px solid #ffdb58" } : { ...BorderButton }}>
                  <FormControlLabel className="boldButton" control={<Checkbox style={{ display: "none", }} onChange={handleChange} name={"Bold"} />} label={"Bold"} />
                </FormGroup>
                <img src={premiumImg} style={PremiumStyle} />
              </Box>
            </Tooltip>
          </Grid>
          <Grid>
            <Tooltip title={<>25% Premium</>} placement="top">
              <Box mr={2} mb={2} className="position-relative">
                <FormGroup className="borderButton" style={data.Border ? { ...BorderButton, background: "#ffdb57", border: "4px solid #ffdb58" } : { ...BorderButton, border: "3px solid #000" }}>
                  <FormControlLabel className="boderBut" control={<Checkbox style={{ display: "none" }} onChange={handleChange} name={"Border"} />} label={"Border"} />
                </FormGroup>
                <img src={premiumImg} style={PremiumStyle} />
              </Box>
            </Tooltip>
          </Grid>
          <Grid >
            <Tooltip title={<>Additional Rs.50/-</>} placement="top">
              <Box className="position-relative eye-catcher-btn" >
                <TextField
                  select
                  label={"Eye Catcher"}
                  variant='outlined'
                  color='primary'
                  size='small'
                  fullWidth={true}
                  name={"eyeCatcher"}
                  value={data['eyeCatcher']}
                  style={{ marginBottom: "5px" }}
                  onChange={handleChange}>
                  {data.categoryName != "Matrimonial" ? EyeCatcherData.slice(0, 2).map((item, i) => (
                    <MenuItem key={item.value + i} value={item.value} >
                      {item.key}
                    </MenuItem>
                  )) :
                    EyeCatcherData.map((item, i) => (
                      <MenuItem key={item.value + i} value={item.value} >
                        {item.key}
                      </MenuItem>))
                  }
                </TextField>
                <img src={premiumImg} style={PremiumStyle} />
              </Box>
            </Tooltip>
          </Grid>
        </Grid>
      </Grid>
      <Grid xs={12} sm={5} lg={4}>
        <h2 style={FontWeight}>Preview</h2>
        <Grid style={TitleStyle(data)}>
          {data.title ? data.title : data.category[0].name}
        </Grid>
        <Grid style={DescStyle(data)} >
          {data.eyeCatcher == "heart" ? <span><img src={HeartImg} /></span> : null}
          {data.eyeCatcher == "tick-mark" ? <span><img src={TickImg} /> </span> : null}
          {data.description}
        </Grid>
          <p style={{ color: "rgb(209, 32, 35)", }}>Note: Actual Print may vary from Preview</p>
        <Grid>
          <p>Total Word Count : {data.description ? <strong>{data.description.match(/(\w+)/g).length}</strong> : 0}</p>
         {data.description ? data.description.match(/(\w+)/g).length > 50 ? <p>Extra Word Charge : <strong>Rs.{NumberwithComma(data.extraChargePrice)}</strong>/word</p>:null:null}
        </Grid>
        {/* <Button variant="text" className="translateButton" href="#" onClick={translateText} underline="always" style={{ marginTop: "20px", textDecoration: "underline" }}>
          Translate to HINDI
        </Button> */}
      </Grid>
    </Grid>
  );
};

export default RowAdsType;


const PremiumStyle = { position: "absolute", right: "5px", top: "5px",width:"12px" }
const BorderButton = { border: "3px solid #f4f4f4", borderRadius: "8px" }
const EyeCatchStyle = {
  fontSize: "2em", padding: "1px", marginRight: "8px", float: "left", lineHeight: "normal"
}

